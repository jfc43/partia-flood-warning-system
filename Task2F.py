#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 15:47:10 2017

@author: Jonathan
"""

from floodsystem.datafetcher import fetch_measure_levels
import datetime
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.flood import stations_highest_rel_level


def Task2F():
    """Requirements for Task 2F"""
    
    stations = build_station_list()[:190] + build_station_list()[191:]
    
    update_water_levels(stations)
    
    
    working_stations = stations_highest_rel_level(stations, 5)
        
    dt = 2
    
    
    
    for station, rwl in working_stations:
        dates, levels = fetch_measure_levels(station.measure_id,
                                                 dt=datetime.timedelta(days=dt))
        
        plot_water_level_with_fit(station, dates, levels, 4)
        
Task2F()