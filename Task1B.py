#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 20:52:51 2017

@author: Jonathan
"""

from floodsystem.geo import stations_by_distance

from floodsystem.stationdata import build_station_list

stations = build_station_list()

Cambridge_centre = (52.2053, 0.1218) 


def runTask1B():
    '''Function that returns either the 10 farthest or 10 closest
    stations to Cambridge centre'''
    
    stationlist = stations_by_distance(stations,Cambridge_centre)
    print('10 closest stations:', [(station.name,station.town,distance) 
                for (station, distance) in stationlist][:10])
    print('\n10 farthest stations:', [(station.name,station.town,distance) 
                for (station, distance) in stationlist][-10:])
                
if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    # Run Task1B
    runTask1B()
    
