# -*- coding: utf-8 -*-
"""
Created on Mon Feb 27 22:44:53 2017

@author: johng
"""
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level



def runTask2C():
    
# Build list of stations
    stations = build_station_list()[:70]

    # Update latest level data for all stations
    update_water_levels(stations)
    
    for station, hwl in stations_highest_rel_level(stations, 10):
        print(station.name, station.r_w_l())
    
    

        
if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")

    runTask2C()
