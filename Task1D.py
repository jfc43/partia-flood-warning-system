# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 22:20:54 2017

@author: johng
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station 
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number

def runTask1D():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()
    
    #Prints the number of rivers with atleast one station
    print("Number of rivers with atleast one station: {}".format(len(stations_by_river(stations))))

    #Prints sorted list of first 10 rivers with a station
    print(sorted(rivers_with_station(stations))[:10] )  
    
    #Prints sorted lists of the stations by each river named
    print(sorted(stations_by_river(stations)['River Aire']))
    print(sorted(stations_by_river(stations)['River Cam']))
    print(sorted(stations_by_river(stations)['Thames']))



if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")

    # Run Task1D
    runTask1D()



