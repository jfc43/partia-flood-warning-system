
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 18:00:07 2017
    # Update latest level data for all stations

@author: johng
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold



def runTask2B():
    
# Build list of stations
    stations = build_station_list()

    update_water_levels(stations)
    s = stations_level_over_threshold(stations, 0.8)
    for station, rwl in s:
        print(station.name, rwl)
    

        
if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")

    runTask2B()