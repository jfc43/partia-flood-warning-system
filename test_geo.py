# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 16:16:20 2017

@author: johng
"""

import pytest

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance 
from floodsystem.geo import rivers_with_station 
from floodsystem.geo import stations_by_river
from floodsystem.geo import rivers_by_station_number
from floodsystem.geo import haversine
import random

def test_haversine():
    #Test that haversine function works
    lyon = (45.7597, 4.8422)
    paris = (48.8567, 2.3508)

    assert haversine(lyon, paris) == 392.21671780659625  # in kilometers
    assert haversine(lyon, paris, miles=True) == 243.71209416020253  # in miles

def test_stations_by_distance():
    #Test that distance sort function works
    stations = build_station_list()
    rand_numbers = [random.randrange(len(stations)) for i in range(10)]
    rand_stations = [stations[i] for i in rand_numbers]
    p = (52.2053, 0.1218)
    sorted_stations = stations_by_distance(rand_stations,p)
    print(sorted_stations)
    for i in range(len(sorted_stations)):
        while i < 9:
            assert haversine(sorted_stations[i][0].coord,p) <= haversine(sorted_stations[i+1][0].coord,p) 
            i += 1
    

def test_r_b_s_n():
    
    #test that the number of stations on the thames by the function stations by river equals the top entry of the rbsn funtion
    stations = build_station_list()
    a = len(stations_by_river(stations)['Thames'])
    b = rivers_by_station_number(stations, len(stations))[0][1]
    
    assert a == b
    
test_r_b_s_n()

def test_s_b_r():
    
    #test that the no of stations by the thames from the function equals the number counted by interating over the stations list
    stations = build_station_list()
    a = len(stations_by_river(stations)['Thames'])
    n=0
    for i in range(len(stations)):
        if stations[i].river == 'Thames':
            n +=1
    assert n == a
    
test_s_b_r()

def test_r_w_s():
    
    #test that the first river with a station by the function equals that from the sorted river with function list
    stations = build_station_list()
    n=0
    a=[]
    for i in range(len(stations)):
        if len(stations[i].river) >= 1:
            a.append(stations[i].river)
    b = sorted(a)
    if b[0] == sorted(rivers_with_station(stations))[0]:
        n = 1
    
    assert n == 1
            
test_r_w_s()