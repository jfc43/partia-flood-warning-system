# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 01:42:41 2017

@author: johng
"""


import pytest
import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.plot import plot_water_levels
import random

def test_plot_water_levels():
    stations = build_station_list()
    update_water_levels(stations)
    station = stations[random.randrange(100)]
    dt = 10
    
    dates, levels = fetch_measure_levels(station.measure_id,
                                         dt=datetime.timedelta(days=dt))
    
    for a in levels:
        assert a >= 0
        
    for b in dates:
        assert b != None
    