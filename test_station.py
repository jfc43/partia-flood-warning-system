"""Unit test for the station module"""


from floodsystem.station import MonitoringStation
from floodsystem.stationdata import build_station_list, update_water_levels

import random


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

'''def test_inconsistent_stations():
    #Test that inconsistent stations are correctly identified
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (random.random(), random.random())
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    if s.typical_range != None and trange[0] <= trange[1]:
        assert s.typical_range_consistent() == True
    else:
        assert s.typical_range_consistent() == False'''

def test_r_w_l():

    # Build list of stations
    stations = build_station_list()

    for station in stations[:90]:
        if station.latest_level == None:
            assert station.r_w_l() == None
        else:
            if station.latest_level > station.typical_range[1]:
                assert station.r_w_l() > 1
            elif station.latest_level < station.typical_range[1] and station.latest_level > station.typical_range[0]:
                assert station.r_w_l() < 1 and station.r_w_l() > 0
            elif station.latest_level < station.typical_range[0]:
                assert station.r_w_l() < 0
        