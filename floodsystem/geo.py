"""This module contains a collection of functions related to
geographical data.

"""

from floodsystem.utils import sorted_by_key
from math import radians, cos, sin, asin, sqrt

AVG_EARTH_RADIUS = 6371  # in km


def haversine(point1, point2, miles=False):
    """ Calculate the great-circle distance between two points on the Earth surface.
    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.
    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))
    :output: Returns the distance bewteen the two points.
    The default unit is kilometers. Miles can be returned
    if the ``miles`` parameter is set to True.
    """
    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * asin(sqrt(d))
    if miles:
        return h * 0.621371  # in miles
    else:
        return h # in kilometers

def stations_by_distance(stations, p):
    # Function that returns list of tuples of stations and distances
    stationdistances = sorted_by_key([(i, haversine(p,i.coord)) for i in stations],1)
    return stationdistances

def stations_within_radius(stations, centre, r):
    ''' Function that returns list of stations within radius r of centre
    by filtering the list created by the stations_by_distance function'''
    return list(filter(lambda stationdistance: stationdistance[1] < r, stations_by_distance(stations, centre)))
    
def rivers_with_station(stations):
    return {station.river for station in stations}

def stations_by_river(stations):
    
    #create an empty dictionary
    a = {}

    #iterate over stations and add the river names to the dictionary as keys with empty lists as values
    for station in stations:
        
        n = []
        a[station.river] = n

    #iterate over stations, if the river exists as a key add the stations to the empty list value
    for station in stations:
        if station.river in a:
            a[station.river].append(station.name)
            
            
    return a

    
def rivers_by_station_number(stations, N):
            
    #create a list of the number of stations per river
    a =[]        
    for key in stations_by_river(stations):
        a.append(len(stations_by_river(stations)[key]))

    #removes duplicates form the list then sorts and cuts it to the highest N values
            
    adupli = []
    seen = set()
    for value in a:
        # If value has not been encountered yet add it to list and set
        if value not in seen:
            adupli.append(value)
            seen.add(value)
    
    asorted = sorted(adupli)

    acut = asorted[-N:]

    #create a dictionary mapping no. of stations per river to that river
    e = {}
    for key in stations_by_river(stations):
        if len(stations_by_river(stations)[key]) not in e:
            e[len(stations_by_river(stations)[key])] = [key]
        elif len(stations_by_river(stations)[key]) in e:
            e[len(stations_by_river(stations)[key])].append(key)

    #iterate over the N highest values and map to their rivers to create a sorted list d     
    #also splits up where the dictionary value is a list with multiple items
    
    d = []
    for i in range(len(acut)):
        if len(e[acut[-i-1]])> 1:
            for n in range(len(e[acut[-1-i]])):
                c = (e[acut[-i-1]][n], acut[-i-1])
                d.append(c)
        else:        
            c = (e[acut[-1-i]][0], acut[-1-i])
            d.append(c)
        
        
    return d