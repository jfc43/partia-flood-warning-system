#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 15:43:30 2017

@author: Jonathan
"""
import matplotlib
import numpy as np

def polyfit(dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(x- x[0], levels, p)
    poly = np.poly1d(p_coeff)
    return poly, x[0]
    
def polydirection(dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    p_coeff = np.polyfit(x- x[0], levels, p)
    return p_coeff