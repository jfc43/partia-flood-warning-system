#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 19:37:55 2017

@author: Jonathan
"""
import matplotlib.pyplot as plt
import datetime
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list
from floodsystem.analysis import polyfit
import numpy as np
import matplotlib

def station_find(Station_name):
        # Build list of stations
        stations = build_station_list()
        for station in stations:
            if station.name == Station_name:
                return station
                break
    
def plot_water_levels(station):          
    
    dt = 10
    
    dates, levels = fetch_measure_levels(station.measure_id,
                                         dt=datetime.timedelta(days=dt))
    
    
    
    # Plot
    plt.plot(dates,levels, color = 'blue')
    plt.plot([dates[0],dates[-1]],[station.typical_range[0],station.typical_range[0]], color = 'green')
    plt.plot([dates[0],dates[-1]],[station.typical_range[1],station.typical_range[1]], color = 'red')
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
    
    
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.figure()
    
def plot_water_level_with_fit(station, dates, levels, p):
    x = matplotlib.dates.date2num(dates)
    poly, shift = polyfit(dates,levels, p)
    plt.plot(x, levels, '.', color = 'blue')
    x1 = np.linspace(x[0], x[-1], 100)
    plt.plot(x1, poly(x1 - shift), color = 'black')
    plt.plot([dates[0],dates[-1]],[station.typical_range[0],station.typical_range[0]], color = 'green')
    plt.plot([dates[0],dates[-1]],[station.typical_range[1],station.typical_range[1]], color = 'red')
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.title(station.name)
    plt.xticks(rotation=45);
    plt.tight_layout()
    plt.figure()

