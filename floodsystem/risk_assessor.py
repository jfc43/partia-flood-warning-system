#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 01:09:25 2017

@author: Jonathan
"""
from floodsystem.stationdata import  update_water_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.analysis import polydirection
from floodsystem.datafetcher import fetch_measure_levels
import datetime




def risk(stations):
    

    update_water_levels(stations)
    
    l = stations_level_over_threshold(stations, 0)
    
    low_risk = [station for station in stations if station not in l]
    
    
    working_stations = [station for station in stations if station.typical_range_consistent() == True]
    middle = [station for station in working_stations if 0 < station.r_w_l() < 1]
    moderate_risk = []
    upper = [station for station in working_stations if 1 <= station.r_w_l() < 1.5]
    highest = [station for station in working_stations if 1.5 <= station.r_w_l()]
    high_risk = []
    severe_risk = []

    for station in middle:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if polydirection(dates,levels,6)[0] > 0 and station not in low_risk:
            moderate_risk.append(station)
        elif polydirection(dates,levels,6)[0] < 0 and station not in low_risk:
            low_risk.append(station)
    
    for station in upper:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if polydirection(dates,levels,6)[0] > 0 and station not in high_risk:
            high_risk.append(station)
        elif polydirection(dates,levels,6)[0] < 0 and station not in moderate_risk:
            moderate_risk.append(station)
    for station in highest:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if polydirection(dates,levels,6)[0] > 0 and station not in severe_risk:
            severe_risk.append(station)
        elif polydirection(dates,levels,6)[0] < 0 and station not in high_risk:
            high_risk.append(station)

    return low_risk, moderate_risk, high_risk, severe_risk
    