#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 17:48:28 2017

@author: Jonathan
"""

from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import update_water_levels

def stations_level_over_threshold(stations, tol):

    # Update latest level data for all stations
    update_water_levels(stations)
    s= []
    for station in stations:
        if station.typical_range_consistent():
            if station.r_w_l() > tol:
            
                a = (station, station.r_w_l())
                s.append(a)
    
    return s
    
def stations_highest_rel_level(stations, N):
    
    update_water_levels(stations)
    s= []
    for station in stations:
        if station.typical_range_consistent():
            if station.r_w_l() != None:
                a = (station, station.r_w_l())
                s.append(a)

    x = sorted_by_key(s, 1, True)
    return x[:N]