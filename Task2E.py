#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 22:30:23 2017

@author: Jonathan
"""

from floodsystem.plot import plot_water_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def runTask2E():
    """Requirements for Task 2E"""
    
    stations = build_station_list()
    update_water_levels(stations)
    
    working_stations = stations_highest_rel_level(stations, 5)
    
    
    
    
    for i, rwl in working_stations:
        plot_water_levels(i)


runTask2E()