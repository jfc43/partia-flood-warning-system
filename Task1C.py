#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 22:03:54 2017

@author: Jonathan
"""

from floodsystem.geo import stations_within_radius

from floodsystem.stationdata import build_station_list



def runTask1C():
    
    #Function that returns all stations within radius r of Cambridge centre
    stations = build_station_list()

    Cambridge_centre = (52.2053, 0.1218) 
    
    print(sorted([(station.name) for (station,distance) in stations_within_radius(stations, Cambridge_centre, 10)]))
    
if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

    # Run Task1C
    runTask1C()