#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 01:09:25 2017

@author: Jonathan
"""
from floodsystem.risk_assessor import risk

from floodsystem.stationdata import build_station_list
from floodsystem.plot import plot_water_levels



def runTask2G():
    stations = build_station_list()
    
    l, m , h ,s = risk(stations[:50])
    
    low_risks = [station.name for station in l]
    moderate_risks = [station.name for station in m]
    high_risks = [station.name for station in h]
    severe_risks = [station.name for station in s]
    
    print("Low risk stations:", low_risks)
    
    plot_water_levels(l[0])
    
    print('Moderate risk stations:', moderate_risks)
    
    plot_water_levels(m[0])
    
    print('High risk stations:', high_risks)
    
    plot_water_levels(h[0])
    
    print('Severe risk stations:', severe_risks)
    
    plot_water_levels(s[0])
    

runTask2G()
    