# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 01:12:29 2017

@author: johng
"""

import pytest
import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from random import randrange

def test_stations_level_over_threshold():

    stations = build_station_list()
    
    z = randrange(len(stations))
    
    for station in stations_level_over_threshold(stations, z):
        assert isinstance(station, tuple) == True
        
def test_stations_highest_rel_level():
    
    stations = build_station_list()
    
    a = randrange(100)
    
    assert len(stations_highest_rel_level(stations, a)) == a

    for station in stations_highest_rel_level(stations, a):
        assert isinstance(station, tuple) == True
        
        
