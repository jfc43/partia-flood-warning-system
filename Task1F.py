#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 20:05:53 2017

@author: Jonathan
"""

from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list
from operator import attrgetter


def runTaskF():
    stations = build_station_list()
    null_stations = inconsistent_typical_range_stations(stations)
    print(sorted(null_stations, key = attrgetter('name')))
                   
if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")

    # Run Task1F
    runTaskF()
